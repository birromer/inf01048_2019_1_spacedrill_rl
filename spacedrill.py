import pygame


from simulation import Simulation
from gui import GUI
from controller1.controller import Controller as ControllerP1
from controller1.controller import State as StateP1
from controller2.controller import Controller as ControllerP2
from controller2.controller import State as StateP2
from bots import RandomBot, CustomBot
from random import uniform
from numpy import loadtxt
import datetime
import config
import csv

import time

import argparse


def parser() -> (argparse.Namespace, list):
    """
    Parses command line arguments.

    :return: a tuple containing parsed arguments and leftovers
    """
    p = argparse.ArgumentParser(prog='Spacedrill')
    mode_p = p.add_subparsers(dest='mode')
    mode_p.required = True
    p.add_argument('-p1', nargs=1, choices=['controller1', 'controller2', 'custom_bot', 'parasite_bot',
                                            'collector_bot'],
                   help='Selects player 1 controller, default is controller in controller1 folder')
    p.add_argument('-w1', nargs=1,
                   help='Selects player 1 Qtable\'s file, leave empty to generate an empty table', default=[''])
    p.add_argument('-p2', nargs=1, choices=['controller1', 'controller2', 'custom_bot', 'parasite_bot',
                                            'collector_bot'],
                   help='Selects player 2 controller, default is controller in controller2 folder')
    p.add_argument('-w2', nargs=1,
                   help='Selects player 1 Qtable\'s file, leave empty to generate an empty table', default=[''])
    mode_p.add_parser('learn',
                      help='Starts %(prog)s in learning mode. This mode does not render the game to your screen, '
                           'resulting in '
                           'faster learning.\n')
    mode_p.add_parser('learn_gui',
                      help='Starts %(prog)s in learning mode. This mode does render the game to your screen so you '
                           'can observe the learning process\n')
    mode_p.add_parser('evaluate',
                      help='Starts %(prog)s in evaluation mode. This mode runs your AI with the weights/parameters '
                           'passed as parameter \n')
    mode_p.add_parser('play',
                      help='Starts %(prog)s in playing mode. You can control each action of the car using the arrow '
                           'keys of your keyboard.\n')
    mode_p.add_parser('comp',
                      help='Starts %(prog)s in competition mode.\n')

    arguments, leftovers = p.parse_known_args()
    p.parse_args()
    return arguments, leftovers


def play():
    s = Simulation()
    gui = GUI(s)
    c = ControllerP1('')
    while True:
        events = pygame.event.get()
        action = 6
        action_2 = 6

        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    exit()
                if event.key == pygame.K_UP:
                    action = s.drill_1.ACCELERATE
                if event.key == pygame.K_LEFT:
                    action = s.drill_1.LEFT
                if event.key == pygame.K_RIGHT:
                    action = s.drill_1.RIGHT
                if event.key == pygame.K_SPACE:
                    action = s.drill_1.ATTACH
                if event.key == pygame.K_f:
                    action = s.drill_1.DISCHARGE

                if event.key == pygame.K_w:
                    action_2 = s.drill_2.ACCELERATE
                if event.key == pygame.K_a:
                    action_2 = s.drill_2.LEFT
                if event.key == pygame.K_d:
                    action_2 = s.drill_2.RIGHT
                if event.key == pygame.K_e:
                    action_2 = s.drill_2.DISCHARGE

                if event.key == pygame.K_r:
                    s.reset()

        s.frame_step(action, action_2)
        print(s.drill_1.sensors)

        gui.draw()
        time.sleep(1/60)


def episode_to_csv(num_episode, best_score, file_name):
    row = [num_episode, best_score]

    with open(file_name, 'a') as f:
        writer = csv.writer(f)
        writer.writerow(row)


def learn(controller1, controller2, number_of_episodes) -> None:
    s = Simulation(controller1, controller2)
    episode_count = 0
    best_score = float('-inf')
    while episode_count < number_of_episodes:

        s.reset()

        # Initial step
        sensors = s.frame_step(5, 5)

        sensors_1 = s.drill_1.sensors
        sensors_2 = s.drill_2.sensors

        new_state_p1 = StateP1(sensors_1)
        old_state_p1 = StateP1(sensors_1)

        new_state_p2 = StateP2(sensors_2)
        old_state_p2 = StateP2(sensors_2)

        frame_number = 0
        start = True
        while frame_number <= config.EPISODE_LEN:
            action = controller1.take_action(new_state_p1, episode_count)
            action2 = controller2.take_action(new_state_p2, episode_count)
            s.frame_step(action, action2)
            sensors_1 = s.drill_1.sensors
            sensors_2 = s.drill_2.sensors
            old_state_p1 = new_state_p1
            old_state_p2 = new_state_p2
            new_state_p1 = StateP1(sensors_1)
            new_state_p2 = StateP2(sensors_2)

            reward = controller1.compute_reward(
                new_state_p1, old_state_p1, action, frame_number, not frame_number)
            controller1.update_q(new_state_p1, old_state_p1,
                                 action, reward, not frame_number)

            frame_number += 1

        print("episode", episode_count, "score", s.drill_1.resources)

        #episode_to_csv(episode_count, s.drill_1.resources, "alpha_05_gamma_095_rewards_2.csv")

        if s.drill_1.resources >= best_score:
            best_score = s.drill_1.resources
            output = "./params/%s_%d.txt" % (datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S'),
                                             episode_count)
            controller1.q_table.save(output)

        episode_count += 1

    output = "./params/%s_final.txt" % datetime.datetime.fromtimestamp(
        time.time()).strftime('%Y%m%d%H%M%S')
    controller1.q_table.save(output)

    pass


def learn_gui(controller1, controller2, number_of_episodes) -> None:
    sim = Simulation(controller1, controller2)
    episode_count = 0
    best_score = float('-inf')
    gui = GUI(sim)
    while episode_count < number_of_episodes:
        sim.reset()

        # Initial step
        sensors = sim.frame_step(5, 5)

        sensors_1 = sim.drill_1.sensors
        sensors_2 = sim.drill_2.sensors

        new_state_p1 = StateP1(sensors_1)
        old_state_p1 = StateP1(sensors_1)

        new_state_p2 = StateP2(sensors_2)
        old_state_p2 = StateP2(sensors_2)

        frame_number = 0
        while frame_number <= config.EPISODE_LEN:
            pygame.event.get()
            action = controller1.take_action(new_state_p1, episode_count)
            action2 = controller2.take_action(new_state_p2, episode_count)

            sim.frame_step(action, action2)

            sensors_1 = sim.drill_1.sensors
            sensors_2 = sim.drill_2.sensors
            old_state_p1 = new_state_p1
            old_state_p2 = new_state_p2
            new_state_p1 = StateP1(sensors_1)
            new_state_p2 = StateP2(sensors_2)

            reward = controller1.compute_reward(
                new_state_p1, old_state_p1, action, frame_number, False)
            controller1.update_q(
                new_state_p1, old_state_p1, action, reward, False)

            gui.draw()
            time.sleep(1/60)

            frame_number += 1

        print("episode", episode_count, "score", sim.drill_1.resources)

        # episode_to_csv(episode_count, sim.drill_1.resources, "alpha_05_gamma_095_rewards_1.csv")

        if sim.drill_1.resources >= best_score:
            best_score = sim.drill_1.resources
            output = "./params/%s_%d.txt" % (datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S'),
                                             episode_count)
            controller1.q_table.save(output)

        episode_count += 1

    output = "./params/%s_final.txt" % datetime.datetime.fromtimestamp(
        time.time()).strftime('%Y%m%d%H%M%S')
    controller1.q_table.save(output)

    pass


def evaluate(controller_1, controller_2, sim):
    gui = GUI(sim)
    frame_number = 0
    sim.frame_step(5, 5)
    state_p1 = StateP1(sim.drill_1.sensors)
    state_p2 = StateP2(sim.drill_2.sensors)

    while frame_number <= config.EPISODE_LEN:
        pygame.event.get()
        state_p1 = StateP1(sim.drill_1.sensors)
        state_p2 = StateP2(sim.drill_2.sensors)

        q_values_p1 = [controller_1.q_table.get_q_value(
            state_p1, i) for i in (1, 2, 3, 4, 5)]
        q_values_p2 = [controller_2.q_table.get_q_value(
            state_p2, i) for i in (1, 2, 3, 4, 5)]

        action_p1 = q_values_p1.index(max(q_values_p1)) + 1
        action_p2 = q_values_p2.index(max(q_values_p2)) + 1

        sim.frame_step(action_p1, action_p2)
        gui.draw()

        frame_number += 1
    pass


def competition():
    s = Simulation()

    points_controller_1 = 0
    points_controller_2 = 0

    for match_no in range(0, 5):
        player_1 = ControllerP1('controller1/table.txt')
        player_2 = ControllerP2('controller2/table.txt')

        evaluate(player_1, player_2, s)

        if s.drill_1.resources > s.drill_2.resources:
            print('Player 1 wins!! +3 points!')
            points_controller_1 += 3
        elif s.drill_1.resources < s.drill_2.resources:
            print('Player 2 wins!! +3 points!')
            points_controller_2 += 3
        else:
            print('It\'s a tie! +1 point for both!')
            points_controller_1 += 1
            points_controller_2 += 1

        print('Player 1 points: %d\nPlayer 2 points: %d' %
              (points_controller_1, points_controller_2))

        s.reset()

    if points_controller_1 > points_controller_2:
        print('Player 1 wins!')
    elif points_controller_2 > points_controller_1:
        print('Player 2 wins!')
    else:
        print('It\'s a tie!')
    pass


if __name__ == '__main__':
    args, leftovers = parser()

    path_p1 = args.w1[0]

    path_p2 = args.w2[0]

    if args.p1 is None:
        player_1 = ControllerP1(path_p1)
    elif args.p1[0] == 'controller1':
        player_1 = ControllerP1(path_p1)
    elif args.p1[0] == 'controller2':
        player_1 = ControllerP2(path_p1)
    elif args.p1[0] == 'random_bot':
        player_1 = RandomBot(path_p1)
    elif args.p1[0] == 'custom_bot':
        player_1 = CustomBot(path_p1)
    else:
        player_1 = ControllerP1(path_p1)

    if args.p2 is None:
        player_2 = RandomBot(path_p2)
    elif args.p2[0] == 'controller1':
        player_2 = ControllerP1(path_p2)
    elif args.p2[0] == 'controller2':
        player_2 = ControllerP2(path_p2)
    elif args.p2[0] == 'random_bot':
        player_2 = RandomBot(path_p2)
    elif args.p2[0] == 'custom_bot':
        player_2 = CustomBot(path_p2)
    else:
        player_2 = RandomBot(path_p2)

    if str(args.mode) == 'learn':
        learn(player_1, player_2, config.NUMBER_OF_EPISODES)
    elif str(args.mode) == 'learn_gui':
        learn_gui(player_1, player_2, config.NUMBER_OF_EPISODES)
    elif str(args.mode) == 'play':
        play()
    elif str(args.mode) == 'evaluate':
        sim = Simulation()
        evaluate(player_1, player_2, sim)
    elif str(args.mode) == 'comp':
        competition()
