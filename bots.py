from controller_interface import Controller, QTable, State
from random import randint
from math import pow, sqrt


class DummyTable(QTable):
    def __init__(self):
        super().__init__()
        return

    def get_q_value(self, key: State, action: int) -> float:
        return randint(1, 10)

    def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
        return

    @staticmethod
    def load(path: str) -> QTable:
        return QTable()

    def save(self, path: str, *args) -> None:
        return

class RandomBot(Controller):
    def __init__(self,path):
        super().__init__('')
        self.q_table = DummyTable()

    def take_action(self, state: tuple, episode_number) -> int:
        return randint(0, 5)

    def compute_features(self, sensors: tuple) -> tuple:
        return 1, 1

    def learn(self, weights: tuple):
        return

class CollectorBot(Controller):

    def take_action(self, weights: tuple):

        if self.sensors['drill_gas'] < 50:
            align = self.sensors['align_mothership']
        else:
            align = self.sensors['align_asteroid']

        if align:
            return self.ACCELERATE
#        elif self.sensors['drill_touching_mothership']:
#            return self.NOTHING
        else:
            return self.LEFT

    def compute_features(self, sensors: tuple) -> tuple:
        return 1, 1

    def learn(self, weights: tuple):
        return

class CustomBot(Controller):
    def __init__(self,path):
        super().__init__('')
        self.q_table = DummyTable()
    def take_action(self, weights: tuple):

        if self.sensors['drill_gas'] < 50:
            align = self.sensors['align_mothership']
        else:
            align = self.sensors['align_asteroid']

        if align:
            return self.ACCELERATE
#        elif self.sensors['drill_touching_mothership']:
#            return self.NOTHING
        else:
            return self.LEFT

    def compute_features(self, sensors: tuple) -> tuple:
        return 1, 1

    def learn(self, weights: tuple):
        return    
