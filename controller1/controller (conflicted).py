from itertools import product
import controller_interface
from typing import List, Tuple, Any
import random
import math
import re

num_steps = 0

primes = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
          103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193)

class State(controller_interface.State):
	def __init__(self, sensors: dict):
		self.sensors = sensors
		
	def compute_features(self):
		features = ()
		# # # Feature: Distance between the drill and the asteroid
		# # distance_to_asteroid = math.sqrt((self.sensors['drill_position'][0] - self.sensors['asteroid_position'][0]) ** 2 + (self.sensors['drill_position'][1] - self.sensors['asteroid_position'][1]) ** 2)
		# # # diff_distance = previous_distance - distance_to_asteroid
		# # features = features + (distance_to_asteroid,)  # distance to the asteroid

		# # # ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		# # # Feature: Relation of distance to mothership and available gas
		# # distance_to_mother = math.sqrt((self.sensors['drill_position'][0] - self.sensors['drill_mothership_position'][0]) ** 2 + (
				# # self.sensors['drill_position'][1] - self.sensors['drill_mothership_position'][1]) ** 2)
		# # features = features + (distance_to_mother,)  # distance to the mothership related to the available fuel
		# # # ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		features = features + (self.sensors['align_asteroid'],)
		features = features + (self.sensors['align_mothership'],)
		features = features + (self.sensors['drill_gas'],)
		
		maximum_values = [1,1,180]
		minimum_values = [0,0,0]
		binary = [1,1,0]
		
		features = self.discretize_features(features, maximum_values, minimum_values, binary)

		return features
		"""
		This function should take the raw sensor information of the drill (see below) and compute useful features for
		selecting an action.
		The drill has the following sensors:

		:param sensors: contains:
			 'asteroid_position': (x, y)
			 'asteroid_velocity': (n, m)
			 'asteroid_resources': 0 - ???
			 'align_asteroid': 0 or 1
			 'align_mothership': 0 or 1
			 'drill_angle': angle in rad
			 'drill_position': (x, y)
			 'drill_velocity': (n, m)
			 'drill_mothership_position': (x, y)
			 'drill_resources': 0 - ???
			 'drill_touching_asteroid': 0 or 1
			 'drill_touching_mothership': 0 or 1
			 'drill_discharge_cooldown': 0 - COOLDOWN (defined in config.py)
			 'drill_edge_position': (x, y)
			 'drill_gas': 0 - MAX_GAS (defined in config.py)
			 'enemy_1_drill_angle': angle in rad
			 'enemy_1_drill_position': (x, y)
			 'enemy_1_drill_velocity': (n, m)
			 'enemy_1_drill_mothership_position': (x, y)
			 'enemy_1_drill_resources': 0 - 0
			 'enemy_1_drill_touching_asteroid': 0 or 1
			 'enemy_1_drill_touching_mothership': 0 or 1
			 'enemy_1_drill_discharge_cooldown': 0 - COOLDOWN  (defined in config.py)
			 'enemy_1_drill_edge_position': (x, y)
			 'enemy_1_drill_gas': 0 - MAX_GAS (defined in config.py)
		:return: A Tuple containing the features you defined
		"""


	def discretize_features(self, features: Tuple, maximum_values, minimum_values, binary) -> Tuple:
		discretized_features = ()
		value_discretization = 0
		discretized = 0
		discretization_levels = self.discretization_levels()

		for i in range(len(features)):
			if binary[i] != 1:
				value_discretization = (maximum_values[i] + abs(minimum_values[i])) // discretization_levels[i]
				discretized = (features[i] + abs(minimum_values[i])) // value_discretization
				if discretized >= discretization_levels[i]:
					discretized = discretization_levels[i] - 1
				if discretized < 0:
					discretized = 0
				discretized_features = discretized_features + (int(discretized),)
			else:
				if features[i] == 0:
					discretized_features = discretized_features + (0,)
				else:
					discretized_features = discretized_features + (1,)

		return discretized_features


	"""
	This function should map the (possibly continuous) features (calculated by compute features) and discretize
	them.
	:param features
	:return: A tuple containing the discretized features
	"""


	@staticmethod
	def discretization_levels() -> Tuple:
		return (2,2,5)
	"""
	This function should return a vector specifying how many discretization levels to use for each state feature.
	:return: A tuple containing the discretization levels of each feature
	"""


	def get_current_state(self):
		"""
		:return: computes the discretized features associated with this state object.
		"""
		features = self.discretize_features(self.compute_features())
		return features


	@staticmethod
	def get_state_id(discretized_features: Tuple) -> int:
		"""
		Handy function that calculates an unique integer identifier associated with the discretized state passed as
		parameter.
		:param discretized_features
		:return: unique key
		"""

		terms = [primes[i] ** discretized_features[i] for i in range(len(discretized_features))]

		s_id = 1
		for i in range(len(discretized_features)):
			s_id = s_id * terms[i]

		return s_id


	@staticmethod
	def get_number_of_states() -> int:
		"""
		Handy function that computes the total number of possible states that exist in the system, according to the
		discretization levels specified by the user.
		:return:
		"""
		v = State.discretization_levels()
		num = 1

		if len(v) > 0:
			for i in (v):
				num *= i
		else:
			num *= v

		return num


	@staticmethod
	def enumerate_all_possible_states() -> List:
		"""
		Handy function that generates a list with all possible states of the system.
		:return: List with all possible states
		"""

		levels = State.discretization_levels()

		levels_possibilities = [(j for j in range(i)) for i in levels]

		return [i for i in product(*levels_possibilities)]
		
class QTable(controller_interface.QTable):
	def __init__(self):
		self.q_table = {}
	
	def get_q_value(self, key: State, action: int) -> float:
		key_discrete = key.compute_features()
		return self.q_table.get((key_discrete, action))
	
	def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
		key_discrete = key.compute_features()
		self.q_table[(key_discrete, action)] = new_q_value

	def insert_discretized_state(self, state, action, q_value):
		self.q_table[(state, action)] = q_value

	# @staticmethod
	def load(path: str) -> "QTable":
		qtable = QTable()
		file = open(path, "r")
		for line in file.readlines():
			key = ()
			values = re.sub("\(", "", line)
			# values = re.sub("\s", "\$", values)
			
			values = re.split("\)", values)
			
			for value in values[0]:
				if value != "'" and value != "," and value != "!" and value != " " and value != "(" and value != ")" and value != "<" and value != ">":
					key = key + (int(value),)

			action = re.sub("\,", "", values[1])
			action = re.sub("\s", "", action)
			action = int(action)
			q_value = re.sub("\s", "", values[2])
			
			qtable.insert_discretized_state(key, action, float(q_value))
		
		file = open("qtable2.txt" , "w")
		for x,y in qtable.q_table.items():
			file.write(str(x) + " " + str(y) + "\n")
			
		return qtable
	
	def save(self, path: str, *args) -> None:
		file = open("qtable.txt" , "w")
		for x,y in self.q_table.items():
			file.write(str(x) + " " + str(y) + "\n")

class Controller(controller_interface.Controller):
	def __init__(self, q_table_path: str):
		if q_table_path is '':
			self.q_table = QTable()
		else:
			self.q_table = QTable.load(q_table_path)
			
	def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_episode: bool) -> None:
		alpha = 0.1
		gamma = 0.975
		global num_steps
		max_value_action = -100000000000
		#new_state_discrete = new_state.compute_features()
		#old_state_discrete = old_state.compute_features()
		
		for i in range(1,6):
			next_state_value = (self.q_table).get_q_value(new_state, i)
			if next_state_value == None:
				print(new_state)
			if next_state_value > max_value_action:
				max_value_action = next_state_value
				
		if not end_of_episode:
			(self.q_table).set_q_value(old_state, action, (1 - alpha) * (self.q_table).get_q_value(old_state, action) + alpha * (reward + gamma * (max_value_action)))
		else:
			(self.q_table).set_q_value(old_state, action, 0)
	
	def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int, end_of_episode: bool) -> float:
		rewards = 0
		
		difference_resources = old_state.sensors['drill_resources'] - new_state.sensors['drill_resources']
		difference_distance_asteroid = self.calculate_distance(old_state, 'drill_position','asteroid_position') - self.calculate_distance(new_state,'drill_position', 'asteroid_position')		
		difference_position = math.sqrt(pow(old_state.sensors['drill_position'][0] - new_state.sensors['drill_position'][0],2) + pow(old_state.sensors['drill_position'][1] - new_state.sensors['drill_position'][1],2))


		# print(new_state.sensors['drill_gas'])


		if difference_resources < 0:
			rewards += 80

		if difference_distance_asteroid < 0:
			rewards += 40
			
		if new_state.sensors['align_asteroid']:
			rewards += 10
		else:
			rewards += -3

		if abs(difference_position) < 1:
			rewards -= 30 

		if new_state.sensors['drill_gas'] < 50 and not new_state.sensors['align_mothership']:
			rewards -= 80
			
		# if new_state.sensors['align_mothership']:
			# rewards += 1
		# else:
			# rewards += -1
			
		# if end_of_episode:
			# if new_state.sensors['drill_resources'] > new_state.sensors['enemy_1_drill_resources']:
				# rewards += 100
			# else:
				# rewards = -1
		
		# print("rewards " + str(rewards))
		

		return rewards

	"""
	This method is called by the learn() method in simulator.Simulation() to calculate the reward to be given to
	the agent.
	:param new_state: The state the drill just entered
	:param old_state: The state the drill just left
	:param action: the action the drill performed to get in new_state
	:param n_steps: number of steps the drill has taken so far in the current episode
	:param end_of_episode: boolean indicating if an episode timeout was reached
	:return: The reward to be given to the agent
	"""


	def take_action(self, new_state: State, episode_number: int) -> int:
		chosen_action = 0
		current_max_value = -100000
		epsilon = episode_number * 1
		# print(epsilon)
		
		#new_state_discrete = new_state.compute_features()
		
		if len((self.q_table).q_table) == 0:									#INICIALIZA DICIONÁRIO COM VALORES ALEATÓRIOS SE NENHUM TIVER SIDO PASSADO COM A LOAD
			number_states = new_state.get_number_of_states()
			possible_states = new_state.enumerate_all_possible_states()
			for i in range(number_states):
				current_state = possible_states[i]
				for j in range(1,6):
					(self.q_table).insert_discretized_state(current_state, j, random.uniform(1,4))
					
		
		random_number = random.uniform(0, 1)
		if random_number <= 1 - epsilon:
			chosen_action = random.randint(1,5)
		else:
			for i in range(1,6):
				value = (self.q_table).get_q_value(new_state, i)
				if value > current_max_value:
					chosen_action = i
					current_max_value = value
			
		#print(str(new_state_discrete) + " " + str((self.q_table).get_q_value(new_state_discrete, chosen_action)) + " ACTION " + str(chosen_action))

		return chosen_action


	"""
	Decides which action the drill must execute based on its Q-Table and on its exploration policy
	:param new_state: The current state of the drill
	:param episode_number: current episode during the training period
	:return: The action the drill chooses to execute
	"""


	def calculate_distance(self, state, param1, param2):  # calculates the distance between param1 and param2, for example: param1 = drill_position and param2 = asteroid_position
		return math.sqrt((state.sensors[param1][0] - state.sensors[param2][0])**2 + (state.sensors[param1][1] - state.sensors[param2][1]) ** 2)
