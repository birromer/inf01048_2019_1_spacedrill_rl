        asteroid_position = np.array(self.sensors['asteroid_position'])
        drill_position = np.array(self.sensors['drill_position'])
        drill_edge_position = np.array(self.sensors['drill_edge_position'])
        drill_position = np.array(self.sensors['drill_position'])
        drill_mothership_position = np.array(self.sensors['drill_mothership_position'])

        vecDrillAsteroid = asteroid_position - drill_position
        normVecDrillAsteroid =  np.linalg.norm(vecDrillAsteroid)
        normVecDrillAsteroid = 0.000000000001 if normVecDrillAsteroid == 0 else normVecDrillAsteroid
        vecDrillAsteroid = vecDrillAsteroid/normVecDrillAsteroid

        vecDirectionShip = drill_edge_position - drill_position
        normVecDirectionShip =  np.linalg.norm(vecDirectionShip)
        normVecDirectionShip = 0.000000000001 if normVecDirectionShip == 0 else normVecDirectionShip
        vecDirectionShip = vecDirectionShip/normVecDirectionShip

        vec90 = np.array([-vecDirectionShip[1], vecDirectionShip[0]]) 
        normVec90 =  np.linalg.norm(vec90)
        normVec90 = 0.000000000001  if normVec90 == 0 else normVec90
        vec90 = vec90/normVec90

        dotprod = vecDrillAsteroid.dot(vec90)
        print(dotprod)

        vecDrillMothership = drill_mothership_position - drill_position

        angle_drill_asteroid =  math.acos( ( vecDrillAsteroid.dot(vecDirectionShip)) / (self.norm(vecDrillAsteroid) * self.norm(vecDirectionShip)) )
        angle_drill_mothership =  math.acos( ( vecDrillMothership.dot(vecDirectionShip)) / (self.norm(vecDrillMothership) * self.norm(vecDirectionShip)) )
        print(angle_drill_mothership)