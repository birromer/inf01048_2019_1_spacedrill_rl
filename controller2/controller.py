from itertools import product
import controller_interface
from typing import List, Tuple, Any

primes = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
          103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193)


class State(controller_interface.State):
    def __init__(self, sensors: dict):
        self.sensors = sensors

    def compute_features(self) -> Tuple:
        """
        This function should take the raw sensor information of the drill (see below) and compute useful features for
        selecting an action.
        The drill has the following sensors:

        :param sensors: contains:
             'asteroid_position': (x, y)
             'asteroid_velocity': (n, m)
             'asteroid_resources': 0 - ???
             'align_asteroid': 0 or 1
             'align_mothership': 0 or 1
             'drill_angle': angle in rad
             'drill_position': (x, y)
             'drill_velocity': (n, m)
             'drill_mothership_position': (x, y)
             'drill_resources': 0 - ???
             'drill_touching_asteroid': 0 or 1
             'drill_touching_mothership': 0 or 1
             'drill_discharge_cooldown': 0 - COOLDOWN (defined in config.py)
             'drill_edge_position': (x, y)
             'drill_gas': 0 - MAX_GAS (defined in config.py)
             'enemy_1_drill_angle': angle in rad
             'enemy_1_drill_position': (x, y)
             'enemy_1_drill_velocity': (n, m)
                 'enemy_1_drill_mothership_position': (x, y)
             'enemy_1_drill_resources': 0 - 0
             'enemy_1_drill_touching_asteroid': 0 or 1
             'enemy_1_drill_touching_mothership': 0 or 1
             'enemy_1_drill_discharge_cooldown': 0 - COOLDOWN  (defined in config.py)
             'enemy_1_drill_edge_position': (x, y)
             'enemy_1_drill_gas': 0 - MAX_GAS (defined in config.py)
        :return: A Tuple containing the features you defined
        """
        raise NotImplementedError("This method must be implemented")

    def discretize_features(self, features: Tuple) -> Tuple:
        """
        This function should map the (possibly continuous) features (calculated by compute features) and discretize
        them.
        :param features
        :return: A tuple containing the discretized features
        """
        raise NotImplementedError("This method must be implemented")

    @staticmethod
    def discretization_levels() -> Tuple:
        """
        This function should return a vector specifying how many discretization levels to use for each state feature.
        :return: A tuple containing the discretization levels of each feature
        """
        raise NotImplementedError("This method must be implemented")

    def get_current_state(self):
        """
        :return: computes the discretized features associated with this state object.
        """
        features = self.discretize_features(self.compute_features())
        return features

    @staticmethod
    def get_state_id(discretized_features: Tuple) -> int:
        """
        Handy function that calculates an unique integer identifier associated with the discretized state passed as
        parameter.
        :param discretized_features
        :return: unique key
        """

        terms = [primes[i] ** discretized_features[i] for i in range(len(discretized_features))]

        s_id = 1
        for i in range(len(discretized_features)):
            s_id = s_id * terms[i]

        return s_id

    @staticmethod
    def get_number_of_states() -> int:
        """
        Handy function that computes the total number of possible states that exist in the system, according to the
        discretization levels specified by the user.
        :return:
        """
        v = State.discretization_levels()
        num = 1

        for i in (v):
            num *= i

        return num

    @staticmethod
    def enumerate_all_possible_states() -> List:
        """
        Handy function that generates a list with all possible states of the system.
        :return: List with all possible states
        """

        levels = State.discretization_levels()

        levels_possibilities = [(j for j in range(i)) for i in levels]

        return [i for i in product(*levels_possibilities)]


class QTable(controller_interface.QTable):
    def __init__(self):
        """
        This class is used to create/load/store your Q-table. To store values we strongly recommend the use of a Python
        dictionary.
        """

    def get_q_value(self, key: State, action: int) -> float:
        """
        Used to securely access the values within this q-table
        :param key: a State object
        :param action: an action
        :return: The Q-value associated with the given state/action pair
        """
        raise NotImplementedError()

    def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
        """
        Used to securely set the values within this q-table
        :param key: a State object
        :param action: an action
        :param new_q_value: the new Q-value to associate with the specified state/action pair
        :return:
        """
        raise NotImplementedError()

    @staticmethod
    def load(path: str) -> "QTable":
        """
        This method should load a Q-table from the specified file and return a corresponding QTable object
        :param path: path to file
        :return: a QTable object
        """
        raise NotImplementedError()

    def save(self, path: str, *args) -> None:
        """
        This method must save this QTable to disk in the file file specified by 'path'
        :param path:
        :param args: Any optional args you may find relevant; beware that they are optional and the function must work
                     properly without them.
        """
        raise NotImplementedError()


class Controller(controller_interface.Controller):
    def __init__(self, q_table_path: str):
        if q_table_path is '':
            self.q_table = QTable()
        else:
            self.q_table = QTable.load(q_table_path)

    def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_episode: bool) -> None:
        """
        This method is called by the learn() method in simulator.Simulation() to update your Q-table after each action
        is taken.
        :param new_state: The state the drill just entered
        :param old_state: The state the drill just left
        :param action: the action the drill performed to get to new_state
        :param reward: the reward the drill received for getting to new_state
        :param end_of_episode: boolean indicating if an episode timeout was reached
        """
        raise NotImplementedError("This method must be implemented")

    def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int,
                       end_of_episode: bool) -> float:
        """
        This method is called by the learn() method in simulator.Simulation() to calculate the reward to be given to
        the agent.
        :param new_state: The state the drill just entered
        :param old_state: The state the drill just left
        :param action: the action the drill performed to get in new_state
        :param n_steps: number of steps the drill has taken so far in the current episode
        :param end_of_episode: boolean indicating if an episode timeout was reached
        :return: The reward to be given to the agent
        """
        raise NotImplementedError("This method must be implemented")

    def take_action(self, new_state: State, episode_number: int) -> int:
        """
        Decides which action the drill must execute based on its Q-Table and on its exploration policy
        :param new_state: The current state of the drill
        :param episode_number: current episode during the training period
        :return: The action the drill chooses to execute
        """
        raise NotImplementedError("This method must be implemented")